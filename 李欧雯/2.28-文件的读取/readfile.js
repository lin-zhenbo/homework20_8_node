//创建100文件吧,内容自便,读取文件把这100个文件的内容给我拼接起来输出,可以写在一个文件里面也可以输出
//引入模块
let fs = require("fs");
let str = "";
let max = 0;
let min;
function filenum(num) {
    if (!isNaN(num) && num > 0) {
        let arr = [];
        let temp;
        for (let i = 0; i < num; i++) {
            let rd = Math.floor(Math.random() * 100);
            arr[i] = rd;
        } console.log("原数组：" + arr);
        for (let i = 0; i < arr.length; i++) {
            for (let j = 0; j < arr.length - 1; j++) {
                //从小到大
                if (arr[j] > arr[j + 1]) {
                    temp = arr[j + 1];
                    arr[j + 1] = arr[j];
                    arr[j] = temp;
                }
            }
        } console.log("排序后：" + arr);
        for (let i = 0; i < num; i++) {
            let path = `${__dirname}/${i + 1}.txt`;
            let fd = fs.openSync(path, 'w');
            fs.writeSync(fd, arr[i]);
            //读取文件内容
            let read = fs.readFileSync(path);
            str += read;
            fs.closeSync(fd);
            if (min >= arr[i] || min === undefined) {
                min = arr[i];
            } else if (max <= arr[i] && max !== undefined) {
                max = arr[i];
            }
        }
        console.log("拼接：" + str);
        console.log("最大值：" + max);
        console.log("最小值：" + min);
    } else {
        console.log("请重新输入");
    }
} filenum(5)
