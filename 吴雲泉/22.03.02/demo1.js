
let fs=require('fs');
let os = require("os");

let fd=fs.openSync("./str.txt",'a');
for(let n=0;n<50000;n++){
    fs.writeSync(fd,'创建一个大文件\n')
}
fs.closeSync(fd);

let startMes = os.freemem();
let fd1 = fs.openSync("str.txt", 'r');
let bf1 = Buffer.alloc(6.1 * 1024 * 1024);
fs.readSync(fd1, bf1, 0, 6.1 * 1024 * 1024);

let endMes = os.freemem();
console.log(startMes);
console.log(endMes);
console.log('传统方式消耗内存'+(startMes - endMes) /1024/1024);
setTimeout(function(){
    fs.closeSync(fd1);
},1000);


// 流方式

let startMes1 = os.freemem();
let writeStream=fs.createWriteStream('str1.txt',{flag:'a'});
for(let n=0;n<50000;n++){
    writeStream.write('创建一个大文件\n')
}
writeStream.close();
let endMes1 = os.freemem();
console.log('流方式消耗内存'+(startMes1 - endMes1) /1024/1024);

