// 流、读
// createReadStream

let fs = require('fs')
let readStream =  fs.createReadStream('被读文件','编码');

// 事件

//文件打开
readStream.on('open',function(fd){
        console.log("文件已打开！");
})

//读取内容
readStream.on('data',function(chuck){
        console.log("读取的内容为"+chuck.toString());
})

//end 流结束传输
readStream.on('end',function(){
    console.log("流结束了");
});

//close 流关闭，
readStream.on('close',function(){
    console.log("流关闭了");
});

//强制关闭了流，相当于终结了文件流传输
//readStream.close();


let writeStram=fs.createWriteStream('./small3.txt',{flags:'a',autoClose:true});
writeStram.write('失恋不可怕，再来一次\n');
writeStram.write(Buffer.from('失恋不可怕，再来两次\n'));
writeStram.close();

/**
 * pipe 管就是把2个流绑定，从数据源流入到可写流
 */

let readFileStream = fs.createReadStream("./small3.txt");
let writeFileStream = fs.createWriteStream('./small4.txt');
readFileStream.pipe(writeFileStream);