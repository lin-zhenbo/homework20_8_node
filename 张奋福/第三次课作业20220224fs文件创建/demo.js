//作业要求：写一段代码，可以指定的创建多个文件，文件名要有序号，写入的内容随机
let fc = require("fs");
//启用文件管理
function creatfs(num) {
    //第二段代码创建一个方法
    //方法要有num参数，这个参数应该是用来循环，循环创建多少文件，以及文件序号
    if (num != null && num > 0) {
        for (let i = 1; i<=num;i++) { 
            let str ="平生不修善果，只爱杀人放火。忽地顿开金枷，这里扯断玉锁。咦！钱塘江上潮信来，今日方知我是我。";
            //这一段代码是随机填入的内容
            let c=str.charAt(Math.random()*str.length);
            //char这个变量接收了，str这个字符串内部随机位置的字母，*乘以，字符串的长度用来填入新创建文件的随机内容
            let fd=fc.openSync(`${__dirname}/demo${i}.txt`,"w");
            //fs是创建文件并且，设置成可写入的 这一段代码的意思是__dirname变量获取当前模块文件所在目录的完整绝对路径。
            // 意思是在当前目录下创建按照序号的文件，并且是可写入的
            fc.writeFileSync(fd,c)
            //这是写入文件内容
            fc.closeSync(fd);
            //关闭文件
        }
    }
    else{
        console.log("请重新输入");
    }
}
module.exports={
    creatfs:creatfs
}