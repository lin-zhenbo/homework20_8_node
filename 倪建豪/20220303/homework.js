let fs=require('fs')
let http=require('http');
let server=http.createServer((req,res)=>{
    res.setHeader("Content-Type", "text/html; charset=utf-8")
    console.log(req.url);
    if (req.url=='/demo1.txt') {
        res.end(fs.readFileSync('/?name=demo1.txt'))
    }
    if (req.url == '/?name=demo2.txt') {
        res.end(fs.readFileSync('./demo2.txt'))
    }
}).listen(8080)