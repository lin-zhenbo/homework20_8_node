//创建个大文件，分别用传统方式于流方式，再计算下内存的消耗
let fs = require("fs");
let os = require("os");
//传统方式
let startTime = Date.now();
let start = os.freemem();
let fd = fs.openSync("bigFile.txt", 'a');
for (let i = 0; i < 10000; i++) {
    fs.writeSync(fd, "我没事就吃溜溜梅\n");
}
let end = os.freemem();
let diff = ((start - end) / 1024 / 1024);
let endTime = Date.now();
let diffTime = ((endTime-startTime)/1000);
console.log("传统方式的内存消耗：" + diff);
console.log("传统方式的时间消耗：" + diffTime);
fs.closeSync(fd);
//流方式
let startTime2 = Date.now();
let start2 = os.freemem();
let stream = fs.createWriteStream("bigFile2.txt", "utf-8", { flag: "a" });
for (let j = 0; j < 10000; j++) {
    stream.write("你别管我了\n");
}
let end2 = os.freemem();
let endTime2 = Date.now();
let diffTime2 = ((endTime2-startTime2)/1000);
let diff2 = ((start2 - end2) / 1024 / 1024);
console.log("流方式的内存消耗：" + diff2);
console.log("流方式的时间消耗：" + diffTime2);
stream.close();