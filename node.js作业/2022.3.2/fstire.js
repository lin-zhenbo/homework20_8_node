//  创建个大文件，分别用传统方式于流方式，再计算下内存的消耗
let fs = require ('fs');
//传统简单方式内存消耗计算
let startAt = Date.now();
for (let i = 0; i < 50000; i++) {
   let fd = fs.writeFileSync('demo01.txt','! ! ! ! !\n',{flag:'a'});
 
}
let endAt = Date.now();
console.log('简单文件时间消耗为：'+(endAt-startAt));
let os = require("os");
let startMes = os.freemem();
let fd = fs.openSync("demo01.txt",'r');
let bf1 = Buffer.alloc(3.28 * 1024 * 1024);
fs.readSync(fd, bf1, 0, 3.28 * 1024 * 1024);
let endMes = os.freemem();
// console.log(startMes);
// console.log(endMes);
console.log("简单文件内存消耗为："+(startMes - endMes) / 1024 / 1024);
//于流方式内存消耗计算
let startMes1 = os.freemem();
let readStream = fs.createReadStream('demo02.txt','utf-8');
let startOn = Date.now();
let writeStream = fs.createWriteStream('./demo02.txt',{flags:'a',autoClose:true});
for(let i=0;i<20000;i++){
    writeStream.write('啦啦啦啦啦\n');
}
let endOn = Date.now();
let endMes1 = os.freemem();
writeStream.close();
console.log('流方式时间消耗为：'+(endOn-startOn));
console.log("流文件内存消耗为："+(startMes1 - endMes1) / 1024 / 1024);



// console.log(startMes);
// console.log(endMes);





