//创建文件

let fs = require("fs");

let createFile = {
    //创建目录
    createDir:function(dirname){
        //检查目录是否创建
        if(fs.existsSync(dirname)) return;
        fs.mkdirSync(dirname);
    },
    //根据个数创建内容
    createConent: function(num){
        return num + '';
    },
    //创建文件名
    createFileName:function(dir,num){
        return dir+'/2022_02_28_'+num+'.txt';
    },
    createFile:function(fileName,content){
        //打开（创建）文件
        let fd=fs.openSync(fileName,'a');
        //写入内容
        fs.writeSync(fd,content);
        //关闭
        fs.closeSync(fd);
    },
    handle:function(dir,num){
        this.createDir(dir);
        for(let i=1;i<=num;i++){
            let fileName = this.createFileName(dir,i);
            let content = this.createConent(i);
            this.createFile(fileName,content);
        }
    }
}
createFile.handle('./files',100);