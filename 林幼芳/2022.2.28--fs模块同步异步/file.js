let fs=require('fs');
//创建目录
let path='./demo';
//判断是否存在目录
if(!fs.existsSync(path)) {
    fs.mkdirSync(path);
}

//循环创建100个文件
for(let i=1;i<=100;i++){
    //文件名
    let fileName=path+'/'+i+'.txt';
    //打开文件
    let fd=fs.openSync(fileName,'a');
    //写入文件
    fs.writeSync(fd,i);
    //关闭文件
    fs.closeSync(fd);
}