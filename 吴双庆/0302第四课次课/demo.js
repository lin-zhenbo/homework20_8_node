/**
 * 流:水流，电流，流量
 * 
 * 之前的文件读取，都是把文本内容一次性的载入到内存
 * 
 * 
 */
let fs = require("fs");
// let startAt = Date.now()
// for(let k=0;k<50000;k++){
//     //简单文件写入，底层 open  write close 
//     fs.writeFileSync('big1.txt','创建一个大文件，我很大哦！！！！！！！！！\n',{flag:'a'});
// }
// let endAt=Date.now();
// console.log('简单的文件写入花了:'+(endAt-startAt));

// let fd=fs.openSync("./big2.txt",'a');
// for(let n=0;n<50000;n++){
//     fs.writeSync(fd,'创建一个大文件，我很大哦！！！！！！！！！\n')
// }
// fs.closeSync(fd);
// let endAt2=Date.now();
// console.log('啰嗦方式写入文件花了'+(endAt2-endAt));

//证明之前学的，读一个文件，其实都是在内容中开辟一个相应大小的内存去存放这个文件
//let os = require("os");
// let startMes = os.freemem();
// let fd = fs.openSync("big2.txt", 'r');
// let bf3 = Buffer.alloc(6.1 * 1024 * 1024);
// fs.readSync(fd, bf3, 0, 6.1 * 1024 * 1024);
// //console.log(bf3.toString());
// let endMes = os.freemem();
// console.log(startMes);
// console.log(endMes);
// console.log((startMes - endMes) / 1024 / 1024);
// setTimeout(function(){
//     fs.closeSync(fd);
// },1000);

/**
 * 流
 */
/**
 * createReadStream
 * path: PathLike, options?: BufferEncoding | ReadStreamOptions
 * path 文件路径
 * BufferEncoding 编码
 */
let readStream=fs.createReadStream('small.txt','utf-8');

readStream.on('data',function(kuai){
    console.log(kuai.toString());
});//on 在这可以理解为绑定
//close 流关闭，
readStream.on('close',function(){
    console.log("流关闭了");
});
//强制关闭了流，相当于终结了文件流传输
//readStream.close();
//end 流结束传输
readStream.on('end',function(){
    console.log("流结束了");
});
/**
 * 相当于打开文件进行传输
 */
readStream.on("open",function(fd){
    console.log("文件打开了fd"+fd);
})
/**
 * createWriteStream(path: PathLike, options?: BufferEncoding | StreamOptions)
 * path 路径
 * 
 */

let writeStram=fs.createWriteStream('./small3.txt',{flags:'a',autoClose:true});
//write 写入数据 string 和 buffer 都行
writeStram.write('失恋不可怕，再来一次\n');
writeStram.write(Buffer.from('失恋不可怕，再来两次\n'));
writeStram.close();

/**
 * pipe 管就是把2个流绑定，从数据源流入到可写流
 */

let readFileStream = fs.createReadStream("./small3.txt");
let writeFileStream = fs.createWriteStream('./small4.txt');
readFileStream.pipe(writeFileStream);

/**
 * 作业
 * 创建个大文件，分别用传统方式于流方式，再计算下内存的消耗
 */






