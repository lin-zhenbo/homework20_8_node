// 写一段代码,可以指定的创业多个文件，文件名中要有序号，写入的内容可以随机
let fs = require('fs'); //该方法属于fs模块，使用前需要引入fs模块

for(let i = 1;i <= 9;i++){
    let fd = fs.openSync(`第${i}个文件`,`w`);//以读取模式打开文件，如果文件不存在则创建
    //异步写入
    fs.writeSync(fd,`肚子饿了${i+1}`,function(err){
        if(err){
            throw err;
        }
    });
fs.closeSync(fd);
}
