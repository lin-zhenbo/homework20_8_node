//创建个大文件，分别用传统方式于流方式，再计算下内存的消耗
    let fs = require('fs');

    for(let i = 0; i < 20000;i++){
        fs.writeFileSync('./dome1.txt','NG！！！！！\n',{flag:'a'});
    }
//简单文件
    let os = require('os');
    let starAt = os.freemem();
    let fd = fs.openSync('./dome1.txt','r');
    let buff = Buffer.alloc(1.83*1024*1024);
    fs.readSync(fd, buff, 0, 1.83 * 1024 * 1024);
    let endAt = os.freemem();
    console.log("内存消耗："+(starAt - endAt) / 1024 / 1024); 
    fs.closeSync(fd);

 //流
    let starAt1 = os.freemem();
    let readFileStream = fs.createReadStream("./dome1.txt");
    let writeFileStream = fs.createWriteStream('./dome2.txt');
    readFileStream.pipe(writeFileStream);
    let endAt1 = os.freemem();
    console.log("内存消耗："+(starAt1 - endAt1) / 1024 / 1024); 


