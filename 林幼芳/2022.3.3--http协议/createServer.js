/**
 * 创建一个web服务器，根据不同的url 去读取页面,给前端渲染
 */

//引入模块
let http=require('http');

//创建web服务
let server=http.createServer();

//事件监听
server.on('request',function(req,res){
    let content='<h1>404 not found</h1>';
    //取到用户端请求的url地址
    let url=req.url;

    //根据不同的url路径响应不同的内容
    if(url === '/' || url === '/index.html'){
        content='<h1>我是首页</h1>'
    }else if(url === '/about.html'){
        content='<h1>我是内容页</h1>'
    }

    //设置响应头，解决中文乱码问题
    res.setHeader('Content-type','text/html; charset=utf-8');

    //把内容响应给客户端
    res.end(content);
})

//启动服务
server.listen('3000',function(){
    console.log('server running at http://127.0.0.1:3000');
})