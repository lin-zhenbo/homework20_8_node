// 创建个大文件，分别用传统方式于流方式，再计算下内存的消耗
let fs = require('fs');
//引入os模块,os模块提供了和操作系统相关的实例方法和属性
let os=require('os');
//创建一个可读流,编码为utf-8
let readStream=fs.createReadStream('./02/stream.txt','utf-8');
//创建一个可写流
let writeStram=fs.createWriteStream('./02/wstream.txt',{flags:'a'});
//将数据写入到可写流中
for (let i = 0; i < 10000; i++) {
    //写入内容
    writeStram.write('提灯寻猫\n')
}
//开启可读流
readStream.on('open',function(fs){
    console.log("文件打开");
});
let star=os.freemem();
readStream.pipe(writeStram);
console.log(star);
let end=os.freemem();
console.log(end);
console.log("内存占用"+(star-end));


