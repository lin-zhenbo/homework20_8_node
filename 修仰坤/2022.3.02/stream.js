//创建个大文件，分别用传统方式于流方式，再计算下内存的消耗
let fs = require("fs");
let os = require("os");
let startAt = Date.now()
for(let i=0;i<5;i++){
    fs.writeFileSync('big1.txt','我是一个超级大的文件哦！\n',{flag:'a'});
}
let endAt=Date.now();
console.log('文件写入花费了：'+(endAt-startAt));
let startMes = os.freemem();
let fd = fs.openSync("big1.txt",'r');
let bf3 = Buffer.alloc(1.8*1024*1024);
fs.readSync(fd,bf3,0,1.8*1024*1024);
console.log(bf3.toString());
let endMes = os.freemem();
console.log(startMes);
console.log(endMes);
console.log('内存使用了：'+(startMes - endMes)/1024/1024);
setTimeout(function(){
    fs.closeSync(fd);
},1000);