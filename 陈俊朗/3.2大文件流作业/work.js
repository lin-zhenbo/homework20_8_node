let fs = require("fs");
let starAt = Date.now();
for (let i = 0; i < 10000; i++) {
    fs.writeFileSync('big.txt','创建一个大文件\n',{flag:'a'});   
}
let endAt = Date.now();
console.log('内存消耗:'+(endAt-starAt));

let os = require('os');

//流式文件写入
let startmes = os.freemem();
let readStream = fs.createReadStream('./readed.txt');
let writeStream = fs.createWriteStream('./inwrite.txt')
readStream.pipe(writeStream);
let endmes = os.freemem();
console.log("内存消耗了"+(startmes - endmes ) / 1024 / 1024)


// 传统文件写入
let startmes2 =os.freemem();
let  fd  = fs.openSync('traRead.txt','w');
fs.writeSync(fd,"这是传统文件写入");
fs.closeSync(fd)
let endmes2 = os.freemem();
console.log("内存消耗了"+(startmes2 - endmes2)/ 1024 / 1024)

//简答文件写
let startmes3 =os.freemem();
fs.writeFileSync('./easyRead.txt',"这是简单文件写入");
let endmes3 = os.freemem();
console.log("内存消耗了"+(startmes3 - endmes3)/ 1024 / 1024)