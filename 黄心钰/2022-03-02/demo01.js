// 创建个大文件，分别用传统方式于流方式，再计算下内存的消耗


let fs = require("fs");
//开始的时间
let startAT = Date.now();
//创建文件，a是追加
//let fd = fs.openSync("./lu.txt");
//循坏
for(let i = 0; i < 40;i++){
    //写入的内容
    fs.writeFileSync('lu.txt','CXB,鹿晗,EXO,哈哈哈哈 \n',{flag:'a'});
}
//结束
//fs.closeSync(fd);
//最后的时间
let endAt = Date.now();





//传统方式
let os = require("os");
let startMes = os.freemem();
let fd = fs.openSync("lu.txt" ,'r');
let bf = Buffer.alloc(4.42 * 1024 *1024);
fs.readSync(fd,bf,0,4.42 *1024 *1024);
//console.log(bf.toString());
let endMes = os.freemem();
console.log("内存的消耗："+(startMes - endMes) / 1024 / 1024);
console.log("花的时间："+(endAt - startAT));

//流
let startMes1 = os.freemem();
let han = fs.createReadStream("./han.txt");
let cxb = fs.createReadStream("./cxb.txt");
han.pipe(cxb);
let endMes1 = os.freemem();
console.log("内存的消耗：" + (startMes1 - endMes1));