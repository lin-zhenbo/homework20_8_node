let fs =require('fs');

let createFile ={
    // 创建目录
    createDir : function(dirname){
        //目录是否创建
        if(fs.existsSync(dirname)) return;
        fs.mkdirSync(dirname);
    },
    //根据个数创建
    createConent : function(num){
        //number转换为string
        return num + "";
    },
    //创建文件名
    createFileName : function(dir,name){
        return dir + name +'.txt';
    },
    //创建文件
    createFile : function(fileName,conent){
        /**
         * flags:标识 r 只读 w 可写  a(apppend) 追加 不会删除原有的数据
         */
        //打开（创建）文件
        let fd = fs.openSync(fileName,'a');
        //写入内容
        let arr ="abcdhriqofbaqgjhaoihewbqa";
        let n =Math.random()*arr.length;
        let str =arr.charAt(n);
        fs.writeFileSync(fd,conent+ str);
        //关闭
        fs.close(fd);
    },
    //处理流程
    handle : function(dir,num){
        // 创建目录
        this.createDir(dir);
        for(let i =0;i<=num;i++){
            let fileName = this.createFileName(dir,i);
            let conent =this.createConent(i);
            this.createFile(fileName,conent);
        }
    }
    
}

createFile.handle('./files',5);