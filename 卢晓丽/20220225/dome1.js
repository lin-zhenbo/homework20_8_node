//第二次作业

let fs =require("fs");

let createFile=
{  
    //创建目录
    createDir:function() 
    {   

        //检查目录是否存在
        // fs.existsSync (path)方法用于同步检查给定路径中是​​否已存在文件。
        //括号里的path：是将要检查的目录路径；

        //用法：fs.mkdirSync( path, options )
        //path:要在其中创建目录的路径。它可以是字符串，缓冲区等。
        //options:它是一个可选参数，用于确定如何像递归地创建目录等。
        //fs.mkdirSync()方法用于同步创建目录。

        if(fs.existsSync(dirname))
        return;
        fs.mkdirSync(dirname);
    },


    //根据个数去创建内容
   createConent:function(num)
   {
   return num + '';   //未考虑异常 todo number 转成 string 
   },

   //创建文件名
   createFilename:function(dir,num)
   {
    return dir + '20220224'+num+'.txt'
   },


   createFile:function(filename,content)
   {

     /**
         * flags:标识 r 只读 w 可写  a(apppend) 追加 不会删除原有的数据
     */
    
     //打开创建文件
     let fd=fs.openSync(filename,'a');

     fs.writeSync(fd,content);
     fs.closeSync(fd);
   },
   //业务处理
   handle:function(dir,num){
       this.createDir
       for(let i=1;i<=num;i++){

        let filename = this.createFilename(dir,i)
        let content = this.createConent(i);
        this.createFile(filename,content);
       }
   }
}

createFile.handle('./files',10)

