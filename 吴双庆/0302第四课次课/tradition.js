// 创建个大文件，分别用传统方式于流方式，再计算下内存的消耗
let fs = require('fs');
//引入os模块,os模块提供了和操作系统相关的实例方法和属性
let os=require('os');

//打开文件,并且为可z
let fs1 = fs.openSync('./01/demo.txt', 'a');
//在01文件夹中创建一个大文件
for (let i = 0; i < 100000; i++) {
    //写入内容
    fs.writeSync(fs1, '提灯寻猫\n')
}
//关闭文件
fs.closeSync(fs1);
//使用os.freemen()方法
//该方法返回系统的空闲内存量，单位为字节。
//创建一个系统的空闲内存量
let star=os.freemem();
//打开文件,文件状态为只读
let fd = fs.openSync('./01/demo.txt', 'r');
// buffer 的长度,该文件为1.37MBs
let bf = Buffer.alloc(1.37 * 1024 * 1024);
fs.readSync(fd, bf, 0, 1.37 * 1024 * 1024);
//创建读取过程中占用的系统内存
let end=os.freemem();
// fs.closeSync(fd);
console.log("内存占用"+(star-end)/1024/1024);
//关闭文件
fs.closeSync(fd);
