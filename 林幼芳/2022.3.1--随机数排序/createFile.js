let fs=require('fs');

let path='./demo';

//判断是否存在目录
if(!fs.existsSync(path)) {
    fs.mkdirSync(path);
}

//循环创建100个文件
for(let i=1;i<=100;i++){
    //文件名
    let fileName=path+'/'+i+'.txt';
    //打开文件
    let fd=fs.openSync(fileName,'a');
    //写入文件
    //生成随机数并取整数
    // function getRandom(min, max) {
    //     return Math.floor(Math.random() * (max - min + 1)) + min;
    // }
    // let num = getRandom(1, 100);
    let random=Math.floor(Math.random()*10);
    fs.writeSync(fd,random);
    //关闭文件
    fs.closeSync(fd); 
}