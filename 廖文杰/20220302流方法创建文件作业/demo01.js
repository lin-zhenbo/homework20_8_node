let fs = require("fs");
let os = require("os");
//传统方式创建大文件

// for(let i=0;i<50000;i++){
//         //简单文件写入，底层 open  write close 
//          fs.writeFileSync("bigfile.txt",'创建一个大大大大大大大大大大大大大大大大大大大大文件\n',{flag:'a'});
// }
let startMes = os.freemem();
let fd = fs.openSync('bigfile.txt','r');
let bf = Buffer.alloc(3.21*1024*1024);
fs.readSync(fd,bf,0,3.21*1024*1024);
let endMes = os.freemem();
console.log(startMes);
console.log(endMes);
console.log((startMes - endMes) / 1024 / 1024);
//流方式创建大文件
// for(let k = 0; k<50000;k++){
//     let WriteStream = fs.createWriteStream('./bigfile2.txt',{flags:'a',autoClose:true});
//     WriteStream.write('再次创建一个大大大大大大大大大大大大大大大大大大大大文件\n');
// }
let startMes2 = os.freemem();
let fd2 = fs.openSync('bigfile2.txt','r');
let bf2 = Buffer.alloc(122880);
fs.readSync(fd2,bf2,0,122880);
let endMes2 = os.freemem();
console.log(startMes2);
console.log(endMes2);
console.log((startMes2 - endMes2) / 1024 / 1024);