//创建100个文件，内容自便，读取文件把这100个文件拼接起来输出
//启用文件
let fc = require("fs");



function creatfs(num) {
    //第二段代码创建一个方法
    //方法要有num参数，这个参数应该是用来循环，循环创建多少文件，以及文件序号
    if (num != null && num > 0) {
        for (let i = 1; i <= num; i++) {

            let fd = fc.openSync(`${__dirname}/demo${i}.txt`, "w");
            //fs是创建文件并且，设置成可写入的 这一段代码的意思是__dirname变量获取当前模块文件所在目录的完整绝对路径。
            // 意思是在当前目录下创建按照序号的文件，并且是可写入的


            //写入文件
            fc.writeSync(fd, i);
            //关闭文件
            fc.closeSync(fd);

        }
        // 循环读取
        //声明一个空变量用以接收循环结果
        let str='';
        for (let i = 1; i <= num; i++) {
            var k = fc.readFileSync(`${__dirname}/demo${i}.txt`, { flag: 'r' });
            // let fk = fc.openSync(`${__dirname}/aa.txt`, "w");
            // var jg = fc.writeFileSync(`${__dirname}/cc.txt`, { flag: "w" });
            console.log(k.toString());
             // 循环读取
            str=str+(k.toString());

            //第二种方法
            //拼接读取写入文件一次完成
            var dd = fc.writeFileSync(`${__dirname}/dd.txt`,k.toString(),{ flag: "a" });
            // console.log(str);

        }
           
      //打印拼接的内容
        console.log(str);
     
    }
    else {
        console.log("请重新输入");
    }
}
module.exports = {
    creatfs: creatfs
}