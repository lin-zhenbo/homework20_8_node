// <!-- 通过模块调用实现加减乘除，
// 模块a实现加减乘除的业务逻辑并且暴露相关的方法，
// 模块b调用a实现具体的业务 -->
function  jia(a,b){
    return a+b;
}
function jian(a,b) {
    return a-b;
}
function chen(a,b) {
    return a*b;
}
function chu(a,b) {
    return a/b;
}
//暴露
module.exports={
    jia:jia,
    jian:jian,
    chen:chen,
    chu:chu
};