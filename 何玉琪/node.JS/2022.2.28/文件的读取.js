/**
 * 一.回顾
 * 同步: cpu -->指令(写入文件 磁盘要去磁道找文件这速度相对cpu是慢的),cpu 有个阻塞(休息)过程
 * 
 * 异步：cpu -->指令(写入文件 磁盘要去磁道找文件这速度相对cpu是慢的),异步有闭包函数callback,
 * 如果还有其它程序先执行其它程序再回过头执行闭包函数
 * 
 * 二.课程 文件的读取
 *    1.同步的读取
 *    2.异步的读取
 *    3.文件流 todo
 * 
 * 
 */

const { SSL_OP_EPHEMERAL_RSA } = require("constants");
let fs = require("fs");
/**
 * fs.openSync(path[, flags[, mode]]):number
 * path 路劲
 * flags: r  w  a 
 * offset 偏移量bf的key的偏移量
 * 
 * return 返回值就是读取的字符长度
 * 
 * 不合理地方:
 *   文件的长度？要额外的去获取文件的长度
 */

let path ='./read.txt';
let fd=fs.openSync(path,'r');
let info=fs.statSync(path);
//console.log(info);
let bf = Buffer.alloc(info.size);
//console.log(bf);
//各自在自己上测试下,不填写offset和length是否可行?版本问题?
let readNumbers=fs.readSync(fd,bf,0,info.size);
console.log(readNumbers);
//console.log(bf.toString());
//关闭
fs.closeSync(fd);

/**
 * 异步文件的读取 
 */
let pathASync='./readAsync.txt';
/***
 * open() 函数异步打开文件
 * path 路劲
 * flag 标识 r w a
 * callback 回调函数 err 错误对象，fd 文件描述符
 * 
 * 
 */
fs.open(pathASync,'r',function(err,fd){
    if(err){
        console.log(err);
    }else{
        //同步读取文件
        //let info=fs.statSync(path);
        //let bfAsync= Buffer.alloc(info.size)
        // fs.readSync(fd,bfAsync,0,info.size)
        // console.log(bfAsync.toString());
        // //记得关闭哦
        // fs.closeSync(fd);

        //异步读取文件
        //
        // fd: number, 文件描述符
        // buffer: TBuffer, buffer 对象
        // offset: number, 偏移量
        // length: number, 读取的长度
        // position: ReadPosition | null,不管可直接填null
        // callback: (err: NodeJS.ErrnoException | null, bytesRead: number, buffer: TBuffer) => void
        let info=fs.statSync(path);
        let bfAsync= Buffer.alloc(info.size);
        fs.read(fd,bfAsync,0,info.size,null,function(err,lenth,bf2){
            console.log(bfAsync.toString());
            console.log('---------------------------------------------------------------');
            console.log(bf2.toString());
        });
        fs.close(fd,function(err){
            console.log(err)
        })


    }
});
//console.log("我在最下面");

//简单文件读取
/**
 * readFileSync
 * path 路劲
 * option：flag 不能为 w (v10.16.0);
 */
 setTimeout(() => {
    let buffer=fs.readFileSync("./sampleSync.txt",{flag:'r',encoding:'utf8'});
    console.log(buffer.toString());
 }, 2000);

 /**
  * 异步文件的读取
  * readFile
  * 
  */

 //fs.readFile
 /**
  * path: PathOrFileDescriptor,
        options:{
                  encoding?: null | undefined;
                  flag?: string | undefined;
              }           
        callback: (err: NodeJS.ErrnoException | null, data: Buffer) => void
  */


 setTimeout(() => {
    fs.readFile("sampleAsyncRead.txt",{encoding:'utf8'},function(err,bf3){
        console.log(arguments)
        console.log(bf3.toString());
    })
     
 }, 4000);

// 作业
// 创建100文件吧,内容自便,读取文件把这100个文件的内容给我拼接起来输出,可以写在一个文件里面也可以输出

//  1.txt  1
//  2.txt  2
//  3.txt  3


 


 




















