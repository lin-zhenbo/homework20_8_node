//创建个大文件，分别用传统方式于流方式，再计算下内存的消耗
let fs = require("fs");
let os = require("os");
//传统方式
let startTime = Date.now();
let start = os.freemem();
let fd = fs.openSync("bigFile.txt", 'a');
for (let i = 0; i < 20000; i++) {
    fs.writeSync(fd, "真的会蒜Q\n");
}
let end = os.freemem();
let diff = ((start - end) / 1024 / 1024);
let endTime = Date.now();
let diffTime = ((endTime-startTime)/1000);
console.log("传统方式的空闲内存量：" + diff);
console.log("传统方式的时间消耗：" + diffTime);
fs.closeSync(fd);
//流方式
let startTime2 = Date.now();
let start2 = os.freemem();
let stream = fs.createWriteStream("bigFile2.txt", "utf-8", { flag: "a" });
for (let j = 0; j < 20000; j++) {
    stream.write("蒜Q\n");
}
let end2 = os.freemem();
let endTime2 = Date.now();
let diffTime2 = ((endTime2-startTime2)/1000);
let diff2 = ((start2 - end2) / 1024 / 1024);
console.log("流方式的空闲内存量：" + diff2);
console.log("传统方式的时间消耗：" + diffTime2);
stream.close();