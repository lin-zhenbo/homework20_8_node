// 作业
//  * 创建个大文件，分别用传统方式于流方式，再计算下内存的消耗


let fs = require('fs');

let os = require('os');
let startMes = os.freemem();

let fd = fs.openSync('./bigger.txt','w');
let str = '';

for (let i = 0; i < 100000; i++) {
    str+='哈里凯恩';
}
let start = os.freemem();
fs.writeSync(fd,str);
fs.closeSync(fd);
let end = os.freemem();
console.log('传统'+(start-end)/1024/1024);

let readStream = fs.createWriteStream('./bigger2.txt',{flags:'w',autoClose:true});

let start2 = os.freemem();
readStream.write(str);
readStream.close();
let end2 = os.freemem();
console.log('流'+(start2-end2)/1024/1024);

