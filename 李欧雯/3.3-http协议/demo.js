// 创建一个web服务器,根据不同的url 去读取页面,给前端渲染
let http = require("http");
let serve = http.createServer(function(req,res){
    res.setHeader('Content-type','text/html; charset=utf-8')
    let url = req.url;
    console.log(url);
    if(url == "/"){
        res.end("来测测你是谁吧");
    }else if(url == "/pig"){
        res.end("哈哈哈你是猪");
    }else if(url == "/dog"){
        res.end("哈哈哈你是狗")
    }
})
serve.listen(3000,function(){
    console.log("服务已启动：http://127.0.0.1:3000");
});