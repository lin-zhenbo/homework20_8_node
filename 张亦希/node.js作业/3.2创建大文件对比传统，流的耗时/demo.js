//创建大文件
let fs = require('fs');

//传统方式创建

let os =require('os');
let starMes = os.freemem();//当前空闲内存
let starAt = Date.now();//开始时间戳
let fd = fs.openSync('./big1.txt','a');//打开文件
for(let i = 0 ;i < 50000 ; i++){
    fs.writeSync(fd,'这个文件很大哦！！！\n');//写入
}

let endMes = os.freemem();//当前空闲内存
console.log(`传统所用的内存：${(starMes - endMes)/1024/1024}`);//用的内存
fs.closeSync(fd);//关闭文件
let endAt = Date.now();//结束时间戳
console.log(`传统所用的时间：${(endAt - starAt)}毫秒`);//用的时间

//流的方式创建
let starMes1 = os.freemem();//当前空闲内存
let starAt1 = Date.now();//开始时间戳
let writeStram = fs.createWriteStream('./big2.txt');

for(let i = 0 ;i < 50000 ; i++){
    writeStram.write('这个文件很大哦！！！\n');
}
let endMes1 = os.freemem();//当前空闲内存
console.log(`流所用的内存：${(starMes1 - endMes1)/1024/1024}`);//用的内存
writeStram.close();
let endAt1 = Date.now();//结束时间戳
console.log(`流所用的时间：${(endAt1 - starAt1)}毫秒`);//用的时间



