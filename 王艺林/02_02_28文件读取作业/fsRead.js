// 引入fs模块
let fs = require('fs');
// 定义一个空字符串的变量
let content = '';
// 创建100个文件
for (let i = 1; i <= 100; i++) {
    // 新建的文件路径名为循环的数字 标识为 ‘w’ 可写
    let fd = fs.openSync(`./${i}.txt`, 'w');
    // 同步写入文件内容  内容为i
    fs.writeSync(fd, i);
    // 关闭文件
    fs.closeSync(fd);
    // 循环读取1-100.txt文件里的内容 
    let bf = fs.readFileSync(`${i}.txt`, {encoding: 'utf8' });
    // 循环拼接到content里;
    content += bf;
}
// 新建一个文件接收拼接的内容
let fd1 = fs.openSync('./content.txt', 'w');
// 同步写入拼接后的内容
fs.writeSync(fd1, content);
// 关闭文件
fs.closeSync(fd1);
// 读取拼接的文件
let join = fs.readFileSync('./content.txt', { encoding: 'utf8' });
console.log(join);