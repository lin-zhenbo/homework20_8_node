//创建自定义数量的文件，随机插入一个随机数的内容，利用a.txt追加所有随机数内容。
let fs= require('fs')
let func=function(num){
    for(let i=1;i<=num;i++){
        let fg=fs.openSync('./a.txt','a')
        let fd=fs.openSync(`./File${i}.txt`,'w')
        let content=Math.ceil(Math.random()*10)
        fs.writeSync(fd,`${content}`)
        fs.writeSync(fg,`${content} `)
        fs.closeSync(fd)
    }
}
//调用函数
func(3);
//fs读取a.txt内容，在终端输出。
let b=fs.readFileSync('./a.txt',{flag:'r',encoding:'utf8'})
console.log(b.toString());
//自定义修改某个序号文件的内容和方式

let func1=function(num1,index,mode){
    let fh=fs.openSync(`./File${num1}.txt`,`${mode}`)
    fs.writeSync(fh,`${index}`)
    //输出修改后该文件的内容在终端显示
    let c=fs.readFileSync(`./File${num1}.txt`,{flag:'r',encoding:'utf8'})
    console.log(c.toString());
}
//调用函数添加参数

func1(2,'吃葡萄不吐葡萄皮','w')
   

